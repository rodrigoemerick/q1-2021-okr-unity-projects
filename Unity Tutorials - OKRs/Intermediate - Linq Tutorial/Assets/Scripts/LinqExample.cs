﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LinqExample : MonoBehaviour
{
    [Header("Any")]
    [SerializeField] string[] m_NamesAny;
    [SerializeField] string m_NameToFind;

    [Header("Distinct")]
    [SerializeField] string[] m_NamesDistinct;

    [Header("Where")]
    [SerializeField] string[] m_NamesWhere;

    [Header("Order by descending")]
    [SerializeField] int[] m_NumbersDescending;

    void Start()
    {
        AnyExample(m_NamesAny, m_NameToFind);
        DistinctExample(m_NamesDistinct);
        WhereExample(m_NamesWhere);
        OrderByDescendingExample(m_NumbersDescending);
    }

    void AnyExample(string[] names, string nameToFind)
    {
        var nameFound = names.Any(name => name == nameToFind);
        Debug.Log($"Any example name found? {nameFound}");
    }

    void DistinctExample(string[] names)
    {
        var uniqueNames = names.Distinct();

        foreach (var name in uniqueNames)
        {
            Debug.Log($"Distinct result name: {name}");
        }
    }

    void WhereExample(string[] names)
    {
        var result = names.Where(name => name.Length > 5);

        foreach (var name in result)
        {
            Debug.Log($"Where result name: {name}");
        }
    }

    void OrderByDescendingExample(int[] numbers)
    {
        var ordered = numbers.OrderByDescending(n => n);

        foreach (var number in ordered)
        {
            Debug.Log($"Descending-ordered number: {number}");
        }
    }
}