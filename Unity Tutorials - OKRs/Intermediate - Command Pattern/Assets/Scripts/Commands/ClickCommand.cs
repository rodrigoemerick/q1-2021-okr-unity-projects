﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickCommand : ICommand
{
    private GameObject m_Cube;
    private Color m_Color;
    private Color m_PreviousColor;

    public ClickCommand(GameObject cube, Color color)
    {
        m_Cube = cube;
        m_Color = color;
    }

    public void Execute()
    {
        m_PreviousColor = m_Cube.GetComponent<MeshRenderer>().material.color;
        m_Cube.GetComponent<MeshRenderer>().material.color = m_Color;
    }

    public void Undue()
    {
        m_Cube.GetComponent<MeshRenderer>().material.color = m_PreviousColor;
    }
}