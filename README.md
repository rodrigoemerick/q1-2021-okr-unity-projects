# Q1 2021 OKR Unity Projects

This repository contains all the projects done by me as a goal of my OKRs from Q1 2021, with intermediate and advanced Unity tutorials implementing something as an Unity project. Details of each project are described below. The classification of intermediate and advanced follows the projects available on https://learn.unity.com/search/?k=%5B%22tag%3A5814655a090915001868ebec%22%2C%22lang%3Aen%22%2C%22t%3Aproject%22%5D 

## Intermediate projects  
### Project 1 - 3D Camera Controller  

In this project, there's an implementation of a basic 3D Camera Controller that follows the player simulationg the behaviour of a Third Person camera movement. The camera has some properties as distance from player, movement speed and height. Everything is done in a single script that is a component of the Main Camera. To test it, in the scene you can move freely using WASD keys in a ground to see the camera behaviour working.

### Project 2 - Command Pattern  

In this project, the Command design pattern is implemented with a very simple example. There are three cubes in the scene and it's possible to interact with them with the mouse left click. When you interact with a cube, a random color is assigned to it and them this "command" is saved. If you interact with the cubes many times, it's easier to see the example working. In the UI, four buttons are displayed: "Play" will play the sequence you clicked the buttons, so you can check every command you done to them; "Rewind" has the same behaviour as "Play" but it'll do backwards; "Done" will make the program understand that you finished editing the cubes, sou you can press "Play" to see your sequence and; "Reset" will clear all the progress you made and reset the cubes. This is an example of how the command pattern is good to isolate functionalities and reducing code dependence.

### Project 3 - Linq Basics Tutorial  

In this project, some methods using Linq commands are used. Basically, in the editor you have a script with some arrays of strings containing names (using the same names as examples but in different arrays), each array is used for a specific method implemented by code, with the output printed in the Console. The implemented methods using Linq are: Any, Distinct, Where and Order by Descending (the last one uses an array of integers instead of strings as example).
