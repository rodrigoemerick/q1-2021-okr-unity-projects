﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTracker : MonoBehaviour
{
    [SerializeField] Transform trackedObject;
    [SerializeField] float maxDistance = 10.0f;
    [SerializeField] float moveSpeed = 20.0f;
    [SerializeField] float updateSpeed = 10.0f;
    [Range(0.0f, 10.0f)]
    [SerializeField] float currentDistance = 5.0f;

    string moveAxis = "Mouse ScrollWheel";
    GameObject ahead;
    MeshRenderer _renderer;
    [SerializeField] float hideDistance = 1.5f;

    void Start()
    {
        ahead = new GameObject("ahead");
        _renderer = trackedObject.gameObject.GetComponent<MeshRenderer>();
    }

    void LateUpdate()
    {
        ahead.transform.position = trackedObject.position + trackedObject.forward * (maxDistance * 0.25f);
        currentDistance += Input.GetAxisRaw(moveAxis) * moveSpeed * Time.deltaTime;
        currentDistance = Mathf.Clamp(currentDistance, 0.0f, maxDistance);
        transform.position = Vector3.MoveTowards(transform.position, trackedObject.position + Vector3.up * currentDistance - trackedObject.forward * (currentDistance + maxDistance * 0.5f), updateSpeed * Time.deltaTime);
        transform.LookAt(ahead.transform);
        _renderer.enabled = (currentDistance > hideDistance);
    }
}