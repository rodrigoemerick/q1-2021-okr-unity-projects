﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CommandManager : MonoBehaviour
{
    private static CommandManager m_Instance;
    public static CommandManager Instance { get { if (m_Instance == null) Debug.LogError("The Command Manager is NULL"); return m_Instance; } }

    private List<ICommand> m_CommandBuffer = new List<ICommand>();

    void Awake()
    {
        m_Instance = this;    
    }

    public void AddCommand(ICommand command)
    {
        m_CommandBuffer.Add(command);
    }

    public void Play()
    {
        StartCoroutine(PlayRoutine());
    }

    public void Rewind()
    {
        StartCoroutine(RewindRoutine());
    }

    public void Done()
    {
        var cubes = GameObject.FindGameObjectsWithTag("Cube");
        foreach(var cube in cubes)
        {
            cube.GetComponent<MeshRenderer>().material.color = Color.white;
        }
    }

    public void Reset()
    {
        m_CommandBuffer.Clear();
        Done();
    }

    IEnumerator PlayRoutine()
    {
        WaitForSeconds delay = new WaitForSeconds(1.0f);

        Debug.Log("Playing...");

        foreach(var command in m_CommandBuffer)
        {
            command.Execute();
            yield return delay;
        }

        Debug.Log("Finished");
    }

    private IEnumerator RewindRoutine()
    {
        WaitForSeconds delay = new WaitForSeconds(1.0f);

        foreach (var command in Enumerable.Reverse(m_CommandBuffer))
        {
            command.Undue();
            yield return delay;
        }
    }
}